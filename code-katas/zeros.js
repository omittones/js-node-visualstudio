var _ = require('underscore');
   
module.exports = function zeros(end) {
    if (end && typeof end === 'number') {
        var count = 0;
        while (end >= 1) {
            end = end / 5;
            count += Math.floor(end);
        }
        return count;
    }
    return 0;
};