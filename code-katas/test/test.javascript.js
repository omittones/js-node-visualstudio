﻿var should = require('should');

describe('scope', function() {

    it('is created by functions', function() {

        var i = 1;
        i.should.eql(1);
        function createScope() {
            var i = 2;
            i.should.eql(2);
        }

        createScope();

        i.should.eql(1);

    });

    it('is not created by if-else', function() {
        var a = 1;
        a.should.eql(1);
        if (true) {
            var a = 2;
            a.should.eql(2);
        }
        a.should.eql(2);
    });

});

describe('variables', function() {
    
    it('should be accessible from inner scope', function() {

        var i = 1;
        i.should.eql(1);
        (function () {
            i = 3;
            i.should.eql(3);
            return;
        })();
        i.should.eql(3);

    });

    it('are hoisted', function () {

        var i = 1;
        i.should.eql(1);
        (function () {
            i = 3;
            i.should.eql(3);
            return;
            var i = 2;
        })();

        i.should.not.eql(3);
        i.should.eql(1);

    });

});
