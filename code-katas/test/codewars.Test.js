﻿(function(module) {

    module.exports = {
        assertEquals: function(a, b, message) {
            this.expect(a === b, message || 'test fail, expected ' + a + ' but got ' + b + '!');
        },
        expect: function(b, message) {
            if (b !== true) {
                throw (message || 'test fail!');
            }
        }
    };

})(module);