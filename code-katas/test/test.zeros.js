var zeros = require('../zeros');
var Test = require('./codewars.Test');

describe('zeros should work for', function() {
    this.timeout(6000);

    var tests = [2, 3, 4, 5, 10, 12, 1000, 1000000000];
    var checks = [0, 0, 0, 1, 2, 2, 249, 249999998];

    tests.forEach(function(number, index) {
        it(number.toString(), function () {
            var check = checks[index];
            if (typeof check === 'function')
                Test.expect(check(zeros(number)));
            else
                Test.assertEquals(zeros(number), check);
        });
    });

});